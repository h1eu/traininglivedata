package com.example.traininglivadata.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.traininglivadata.R
import com.example.traininglivadata.databinding.ActivityMainBinding
import com.example.traininglivadata.databinding.FragmentControlBinding
import com.example.traininglivadata.viewmodel.MedalViewModel

class ControlFragment : Fragment() {
    private lateinit var binding: FragmentControlBinding
    private lateinit var mMedalViewModel: MedalViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentControlBinding.inflate(layoutInflater, container, false)
        //val view = inflater.inflate(R.layout.fragment_control, container, false)
        if (activity != null) {
            mMedalViewModel = ViewModelProvider(requireActivity()).get(MedalViewModel::class.java)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpDisplayMetal()
        handle()
    }

    private fun setUpDisplayMetal() {
        binding.tvGoldNumber.text = mMedalViewModel.goldMedal.value.toString()
        binding.tvSilverNumber.text = mMedalViewModel.silverMedal.value.toString()
        binding.tvBronzeNumber.text = mMedalViewModel.bronzeMedal.value.toString()
    }

    private fun displayMetal() {
        binding.tvGoldNumber.text = mMedalViewModel.goldMedal.value.toString()
        binding.tvSilverNumber.text = mMedalViewModel.silverMedal.value.toString()
        binding.tvBronzeNumber.text = mMedalViewModel.bronzeMedal.value.toString()
    }

    private fun handle() {
        binding.imgGoldAdd.setOnClickListener(View.OnClickListener {
            mMedalViewModel.goldMedal.value = mMedalViewModel.goldMedal.value?.plus(1)
            displayMetal()
        })
        binding.imgGoldAdd.setOnClickListener(View.OnClickListener {
            if (mMedalViewModel.goldMedal.value!! > 0) {
                mMedalViewModel.goldMedal.value = mMedalViewModel.goldMedal.value?.minus(1)
                displayMetal()
            }
        })
        binding.imgSilverAdd.setOnClickListener(View.OnClickListener {
            mMedalViewModel.silverMedal.value = mMedalViewModel.silverMedal.value?.plus(1)
            displayMetal()
        })
        binding.imgSilverMinus.setOnClickListener(View.OnClickListener {
            if (mMedalViewModel.silverMedal.value!! > 0) {
                mMedalViewModel.silverMedal.value = mMedalViewModel.silverMedal.value?.minus(1)
                displayMetal()
            }
        })
        binding.imgBronzeAdd.setOnClickListener(View.OnClickListener {
            mMedalViewModel.bronzeMedal.value = mMedalViewModel.bronzeMedal.value?.plus(1)
            displayMetal()
        })
        binding.imgBronzeMinus.setOnClickListener(View.OnClickListener {
            if (mMedalViewModel.bronzeMedal.value!! > 0) {
                mMedalViewModel.bronzeMedal.value = mMedalViewModel.bronzeMedal.value?.minus(1)
                displayMetal()
            }
        })
    }


}