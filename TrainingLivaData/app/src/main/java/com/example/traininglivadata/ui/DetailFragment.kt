package com.example.traininglivadata.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.traininglivadata.R
import com.example.traininglivadata.databinding.FragmentDetailBinding
import com.example.traininglivadata.viewmodel.MedalViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : Fragment() {
    private lateinit var binding : FragmentDetailBinding
    private lateinit var mMetalViewModel : MedalViewModel
    private var nMedalGold = 0
    private var nMedalSilver = 0
    private var nMedalBronze = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater,container,false)
        mMetalViewModel = ViewModelProvider(requireActivity()!!).get(MedalViewModel::class.java)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMetalViewModel.goldMedal.observe(requireActivity()!!, Observer<Int>{display()})
        mMetalViewModel.silverMedal.observe(requireActivity()!!,Observer<Int>{display()})
        mMetalViewModel.bronzeMedal.observe(requireActivity()!!,Observer<Int>{display()})
        setupDisplay()
        display()
    }

    private fun display() {
        nMedalGold = mMetalViewModel.goldMedal.value!!
        nMedalSilver = mMetalViewModel.silverMedal.value!!
        nMedalBronze = mMetalViewModel.bronzeMedal.value!!
        binding.tvGoleMedal.text = nMedalGold.toString() + " huy chương vàng"
        binding.tvSilverMedal.text = nMedalSilver.toString() + " huy chương bạc"
        binding.tvBronzeMedal.text = nMedalBronze.toString() + " huy chương đồng"
    }

    private fun setupDisplay() {
        binding.tvGoleMedal.text = nMedalGold.toString() + " huy chương vàng"
        binding.tvSilverMedal.text = nMedalSilver.toString() + " huy chương bạc"
        binding.tvBronzeMedal.text = nMedalBronze.toString() + " huy chương đồng"
    }

}