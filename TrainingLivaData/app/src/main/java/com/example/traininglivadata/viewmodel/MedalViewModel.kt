package com.example.traininglivadata.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MedalViewModel : ViewModel() {
    private var _goldMedal : MutableLiveData<Int> = MutableLiveData()
    private var _silverMedal : MutableLiveData<Int> = MutableLiveData()
    private var _bronzeMedal : MutableLiveData<Int> = MutableLiveData()
    init {
        _goldMedal.value = 0
        _silverMedal.value = 0
        _bronzeMedal.value = 0
    }

    var goldMedal = _goldMedal
    var silverMedal = _silverMedal
    var bronzeMedal = _bronzeMedal

}