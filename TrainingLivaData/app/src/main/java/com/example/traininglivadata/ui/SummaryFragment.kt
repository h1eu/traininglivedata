package com.example.traininglivadata.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.traininglivadata.R
import com.example.traininglivadata.databinding.FragmentSummaryBinding
import com.example.traininglivadata.viewmodel.MedalViewModel


class SummaryFragment : Fragment() {
    private lateinit var binding : FragmentSummaryBinding
    private lateinit var medalViewModel: MedalViewModel
    private var toTalMedal = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSummaryBinding.inflate(layoutInflater,container,false)
        medalViewModel = ViewModelProvider(requireActivity()).get(MedalViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        medalViewModel.goldMedal.observe(requireActivity(),Observer<Int>{updateMetal()})
        medalViewModel.silverMedal.observe(requireActivity(),Observer<Int>{updateMetal()})
        medalViewModel.bronzeMedal.observe(requireActivity(),Observer<Int>{updateMetal()})
        setUpDisplay()
        updateMetal()

    }

    private fun updateMetal() {
        toTalMedal = medalViewModel.goldMedal.value!!.plus(medalViewModel.silverMedal.value!!).plus(medalViewModel.bronzeMedal.value!!)
        binding.tv.text = toTalMedal.toString() + " huy chương"
    }

    private fun setUpDisplay() {
        binding.tv.text = toTalMedal.toString() + " huy chương"
    }
}